// @flow
import type { Moment } from 'moment';
import type { Plans } from '../../../../flow/general';

const willBeAdjusted = (
    prior: { from: Moment, to: Moment },
    current: { from: Moment, to: Moment },
    shiftMinutes: number,
    direction: 1 | -1
): boolean => {
    /*

        The prior is whatever occured before in the loop, if current is the first dominio, then it is the user adjusted one.
        prior is taken from the adjusted newPlans array.

        WILL BE ADJUSTED IF:
            prev adjusted to will conflict with this from.
        WON'T BE ADJUSTED IF:
            does not infringe.
            activity does not exist.

    */
    if (!current || shiftMinutes <= 0) {
        return false;
    }
    if (direction === 1) {
        // Forwards.
        if (prior.to.valueOf() > current.from.valueOf()) {
            /*
                Will Conflict, thus should adjust.

                This should be the case for activities on separate days as well, as they should not touch.
            */
            return true;
        }

        return false;
    }
    // Backwards.
    if (prior.from.valueOf() < current.to.valueOf()) {
        /*
            Will Conflict, thus should adjust.

            This should be the case for activities on separate days as well, as they should not touch.
        */
        return true;
    }

    return false;
};

export default (
    plans: Plans,
    index: number,
    direction: 1 | -1,
    shiftMinutes: number,
    loc: 'from' | 'to',
    split: boolean = false,
    tripStart: Moment,
    tripEnd: Moment
): ?Plans => {
    /*
        This adjustment 'domino' algorithm works by 'eating' the time of an activity until it's at it's minimum duration of 10 mins.
        In that case, it will continue on to the next one, hence the name. Continues eating activities until the shift is completely dissipated.

        Returns either an array of plans to replace the existing on in redux store, or null to cancel the action.
    */

    // Copy a new array.
    const newPlans = [...plans];

    // Adjust the activity the adjustment is being performed upon exactly how the user wants as long as no conflicts.
    newPlans[index] = {
        ...plans[index],
        [loc]: plans[index][loc].clone().add(shiftMinutes * direction, 'minutes')
    };

    // Check for cases, returning null to cancel action.
    if (newPlans[index].from.valueOf() < tripStart.valueOf()) {
        // Out of bounds, before start.
        return null;
    }
    if (newPlans[index].to.valueOf() > tripEnd.valueOf()) {
        // Out of bounds, after end.
        return null;
    }
    if (newPlans[index].to.diff(newPlans[index].from) > 86400000) {
        // Limit activity duration to max of 24 hrs.
        return null;
    }

    // Adjustable shift.
    let shift = shiftMinutes;

    if (direction === -1 && loc === 'to') {
        // Going UP in the planner and using the 'to' loc slider.

        if (shiftMinutes > plans[index].to.diff(plans[index].from) / 60000 - 10) {
            // Instead of going to negative, keep the current time constant at 10 min and domino up.
            const duration = plans[index].to.diff(plans[index].from) / 60000;
            const possibleEat = duration - 10;
            if (shift > possibleEat) {
                shift -= possibleEat;
                newPlans[index] = {
                    ...newPlans[index],
                    from: plans[index].from.clone().add(shift * direction, 'minutes')
                };
            }
            if (newPlans[index].from.valueOf() < tripStart.valueOf()) {
                return null;
            }
        }
        // This condition will not cause a domino, only for the next plan to have it's from moved back.
        if (!split && plans[index + 1] && plans[index + 1].from.isSame(plans[index].to, 'minute')) {
            const from = plans[index + 1].from.clone().subtract(shiftMinutes, 'minutes');
            newPlans[index + 1] = {
                ...plans[index + 1],
                from
            };
            if (newPlans[index + 1].from.valueOf() < tripStart.valueOf()) {
                return null;
            }
        }
    }
    if (loc === 'from' && direction === 1) {
        // Going DOWN and using the 'from' loc slider.
        // from loc is checked first because for most cases you'll use the to loc slider.
        const duration = plans[index].to.diff(plans[index].from) / 60000;
        const possibleEat = duration - 10;
        if (shift > possibleEat) {
            shift -= possibleEat;
            console.log({ shift, possibleEat });
            newPlans[index] = {
                ...newPlans[index],
                to: plans[index].to.clone().add(shift, 'minutes')
            };
        }

        if (newPlans[index].to.valueOf() > tripEnd.valueOf()) {
            // Out of bounds, before start.
            return null;
        }
        // This condition will not cause a domino, only for the prev plan to have it's to moved forward.
        if (!split && plans[index - 1] && plans[index - 1].to.isSame(plans[index].from, 'minute')) {
            const to = plans[index - 1].to.clone().add(shiftMinutes, 'minutes');
            newPlans[index - 1] = {
                ...plans[index - 1],
                to
            };
            if (newPlans[index - 1].to.valueOf() > tripEnd.valueOf()) {
                // Out of bounds, after end.
                return null;
            }
        }
    }
    // i is the prev or next according to whatever the difrection is.
    let i = index + direction;
    // Domino the rest.
    while (willBeAdjusted(newPlans[i - direction], plans[i], shift, direction)) {
        if (direction === 1) {
            // Forward.
            // Next plan isn't fixed, or is fixed but this won't have an effect.
            const duration = plans[i].to.diff(plans[i].from) / 60000;
            const from = plans[i].from.clone().add(shift, 'minutes');

            const possibleEat = duration - 10;
            const newActivity = {
                ...plans[i],
                from
            };

            if (shift > possibleEat) {
                // Duration cannot be fully eaten.
                // Eat from this activity, and move.
                shift -= possibleEat;
                const to = plans[i].to.clone().add(shift, 'minutes');
                if (to.valueOf() > tripEnd.valueOf()) {
                    return null;
                }
                newActivity.to = to;
            }

            newPlans[i] = newActivity;
            i++;
        } else {
            // Reverse.
            const duration = plans[i].to.diff(plans[i].from) / 60000;

            const to = plans[i].to.clone().subtract(shift, 'minutes');

            const possibleEat = duration - 10;
            const newActivity = {
                ...plans[i],
                to
            };

            if (shift > possibleEat) {
                // Duration cannot be fully eaten.
                // Eat from this activity, and move.
                shift -= possibleEat;
                const from = plans[i].from.clone().subtract(shift, 'minutes');
                if (from.valueOf() < tripStart.valueOf()) {
                    return null;
                }
                newActivity.from = from;
            }

            newPlans[i] = newActivity;
            i--;
        }
    }
    return newPlans;
};
