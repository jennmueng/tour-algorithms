// @flow
import moment from 'moment';

import type { Moment } from 'moment';
import type { Plans } from '../../../../flow/general';

export default (
    oldScope: { from: Moment, to: Moment },
    newScope: { from: Moment, to: Moment },
    plans: Plans // Array of plans { ... from: Moment, to: Moment, ... }
): {
    newPlans: Plans,
    emptyDays: Array<number>, // Array containing pointers of which days are empty.
    filledDays: Array<number>, // Array containing pointers of which days are filled.
    removedDays: Array<number>, // Array containing pointers of which days were removed.
    removedPlans: Array<string> // Array containing id's of activities removed.
} => {
    /*
        Shifts a plans array into a new scope.

        Focuses on retaining the 'pattern' of the plan structure.
        Prioritizes removing empty days from the front until reach first filled day, then keep removing from back.
    */

    // console.log('Old Scope:');
    // console.dir(oldScope);
    // console.log('New Scope:');
    // console.dir(newScope);
    // console.dir(plans);

    const oldDayCount = Math.ceil(moment.duration(oldScope.to.diff(oldScope.from)).asDays());
    const newDayCount = Math.ceil(moment.duration(newScope.to.diff(newScope.from)).asDays());

    // console.dir({ oldDayCount, newDayCount });

    let old: {
        days?: Array<{
            data: Plans,
            lastCrossesDay: boolean,
            count: number
        }>,
        emptyDays?: Array<number>, // pointer array containing the indexes of days that are empty.
        filledDays?: Array<number> // pointer array containing the indexes of days that are filled.
    } = {};
    if (oldDayCount > 0) {
        const days = [];
        let currentDay = 0;
        let i = 0;
        const emptyDays = [];
        const filledDays = [];
        // Get the stuff from old scope.
        while (currentDay < oldDayCount) {
            // Go through each day.

            const dayStart = oldScope.from
                .clone()
                .add(currentDay, 'd')
                .startOf('d');
            const dayEnd = dayStart.clone().endOf('d');
            const data = [];
            let lastCrossesDay = false;
            let count = 0;

            while (i < plans.length && plans[i].from.isSame(dayStart, 'd')) {
                // Get the plans of that day
                const activity = plans[i];
                if (activity.to.isAfter(dayEnd)) {
                    lastCrossesDay = true;
                }
                data.push(activity);
                count++;
                i++;
            }

            if (count === 0) {
                emptyDays.push(currentDay);
            } else {
                filledDays.push(currentDay);
            }
            days.push({
                data,
                lastCrossesDay,
                count
            });
            currentDay++;
            // console.log({ currentDay, i });
        }
        old = {
            days,
            emptyDays,
            filledDays
        };
    }

    // console.dir(old);

    // $FlowFixMe
    const filledDays = [...old.filledDays];
    // $FlowFixMe
    const emptyDays = [...old.emptyDays];
    const removedDays = [];

    if (newDayCount < oldDayCount) {
        // Will have to reduce days.
        const diff = oldDayCount - newDayCount;

        let pointer = true;

        for (let d = 0; d < diff; d++) {
            if (emptyDays.length > 0) {
                // eat empty days.
                if (pointer === true && emptyDays[0] < filledDays[0]) {
                    // the first pointer of the empty arrays is BEFORE the first filled day, otherwise remove from back instead.
                    // remove from front
                    emptyDays.shift();
                    pointer = false;
                } else {
                    // remove from end
                    emptyDays.splice(emptyDays.length - 1, 1);
                    pointer = true;
                }
            } else {
                // eat from the end of filled.
                removedDays.push(filledDays.pop());
            }
        }
    }

    // Merges the two pointer arrays;
    const finalPointers = [...filledDays, ...emptyDays].sort();
    // // console.dir(finalPointers);

    // Takes the merged pointer array, fetches the data for those days, then shifts the date of the activities.
    /* eslint-disable prefer-spread */
    // $FlowFixMe
    const newDays = finalPointers.map(dayIndex => old.days[dayIndex].data).map((day, dayIndex) =>
        day.map((activity, index, array) => {
            const dayAnchor = newScope.from.clone().add(dayIndex, 'd');
            const year = dayAnchor.year();
            const dayOfYear = dayAnchor.dayOfYear();
            const from = activity.from
                .clone()
                .year(year)
                .dayOfYear(dayOfYear);
            let to = activity.to
                .clone()
                .year(year)
                .dayOfYear(dayOfYear);
            if (index === array.length - 1) {
                // Last activity.
                if (to.isAfter(newScope.to)) {
                    // goes over the new scope end.
                    // limit.
                    to = newScope.to.clone().minute(50);
                }
            }
            return {
                ...activity,
                from,
                to
            };
        })
    );

    // // console.dir(newDays);

    const newPlans = [].concat.apply([], newDays);

    const removedPlans = [].concat.apply(
        [],
        removedDays.map(dayIndex => old.days[dayIndex].data).map(day => day.map(activity => activity.id))
    );

    return {
        newPlans,
        emptyDays: emptyDays.map(index => finalPointers.indexOf(index)),
        filledDays: filledDays.map(index => finalPointers.indexOf(index)),
        removedDays,
        removedPlans // id's of removed activities
    };
};
