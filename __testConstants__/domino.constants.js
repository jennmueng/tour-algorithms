import moment from 'moment';

const tripStart = moment.utc().startOf('month');
const tripEnd = tripStart
    .clone()
    .add(2, 'd')
    .endOf('day');

export const tripAttr = {
    tripStart,
    tripEnd
};

export const plans = [
    {
        // Day 1, 9:00 - 10:00
        id: 0,
        from: tripStart.clone().hour(9),
        to: tripStart.clone().hour(10)
    },
    {
        // Day 1, 10:00 - 10:30
        id: 1,
        from: tripStart.clone().hour(10),
        to: tripStart
            .clone()
            .hour(10)
            .minute(30)
    },
    {
        // Day 1, 10:30 - 13:20
        id: 2,
        from: tripStart
            .clone()
            .hour(10)
            .minute(30),
        to: tripStart
            .clone()
            .hour(13)
            .minute(20)
    },
    {
        // Day 2, 6:30 - 9:10
        id: 3,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(6)
            .minute(30),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(9)
            .minute(10)
    },
    {
        // Day 2, 10:30 - 12:00
        id: 4,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(10)
            .minute(30),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 2, 18:00 - 19:00
        id: 5,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(18)
            .minute(0),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(19)
            .minute(0)
    },
    {
        // Day 3, 6:00 - 12:00
        id: 6,
        from: tripStart
            .clone()
            .add(2, 'd')
            .hour(6)
            .minute(0),
        to: tripStart
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 3, 12:00 - 23:50
        id: 7,
        from: tripStart
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0),
        to: tripStart
            .clone()
            .add(2, 'd')
            .hour(23)
            .minute(50)
    }
];

export const result1 = [
    {
        // Day 1, 10:00 - 10:10
        id: 0,
        from: tripStart.clone().hour(10),
        to: tripStart
            .clone()
            .hour(10)
            .minute(10)
    },
    {
        // Day 1, 10:10 - 10:30
        id: 1,
        from: tripStart
            .clone()
            .hour(10)
            .minute(10),
        to: tripStart
            .clone()
            .hour(10)
            .minute(30)
    },
    {
        // Day 1, 10:30 - 13:20
        id: 2,
        from: tripStart
            .clone()
            .hour(10)
            .minute(30),
        to: tripStart
            .clone()
            .hour(13)
            .minute(20)
    },
    {
        // Day 2, 6:30 - 9:10
        id: 3,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(6)
            .minute(30),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(9)
            .minute(10)
    },
    {
        // Day 2, 10:30 - 12:00
        id: 4,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(10)
            .minute(30),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 2, 18:00 - 19:00
        id: 5,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(18)
            .minute(0),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(19)
            .minute(0)
    },
    {
        // Day 3, 6:00 - 12:00
        id: 6,
        from: tripStart
            .clone()
            .add(2, 'd')
            .hour(6)
            .minute(0),
        to: tripStart
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 3, 12:00 - 23:50
        id: 7,
        from: tripStart
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0),
        to: tripStart
            .clone()
            .add(2, 'd')
            .hour(23)
            .minute(50)
    }
];

export const result2 = [
    {
        // Day 1, 9:00 - 11:00
        id: 0,
        from: tripStart.clone().hour(9),
        to: tripStart.clone().hour(11)
    },
    {
        // Day 1, 11:00 - 11:10
        id: 1,
        from: tripStart.clone().hour(11),
        to: tripStart
            .clone()
            .hour(11)
            .minute(10)
    },
    {
        // Day 1, 11:10 - 13:20
        id: 2,
        from: tripStart
            .clone()
            .hour(11)
            .minute(10),
        to: tripStart
            .clone()
            .hour(13)
            .minute(20)
    },
    {
        // Day 2, 6:30 - 9:10
        id: 3,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(6)
            .minute(30),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(9)
            .minute(10)
    },
    {
        // Day 2, 10:30 - 12:00
        id: 4,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(10)
            .minute(30),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 2, 18:00 - 19:00
        id: 5,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(18)
            .minute(0),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(19)
            .minute(0)
    },
    {
        // Day 3, 6:00 - 12:00
        id: 6,
        from: tripStart
            .clone()
            .add(2, 'd')
            .hour(6)
            .minute(0),
        to: tripStart
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 3, 12:00 - 23:50
        id: 7,
        from: tripStart
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0),
        to: tripStart
            .clone()
            .add(2, 'd')
            .hour(23)
            .minute(50)
    }
];

export const result3 = [
    {
        // Day 1, 9:00 - 10:00
        id: 0,
        from: tripStart.clone().hour(9),
        to: tripStart.clone().hour(10)
    },
    {
        // Day 1, 10:00 - 12:30
        id: 1,
        from: tripStart.clone().hour(10),
        to: tripStart
            .clone()
            .hour(12)
            .minute(30)
    },
    {
        // Day 1, 12:30 - 13:20
        id: 2,
        from: tripStart
            .clone()
            .hour(12)
            .minute(30),
        to: tripStart
            .clone()
            .hour(13)
            .minute(20)
    },
    {
        // Day 2, 6:30 - 9:10
        id: 3,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(6)
            .minute(30),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(9)
            .minute(10)
    },
    {
        // Day 2, 10:30 - 12:00
        id: 4,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(10)
            .minute(30),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 2, 18:00 - 19:00
        id: 5,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(18)
            .minute(0),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(19)
            .minute(0)
    },
    {
        // Day 3, 6:00 - 12:00
        id: 6,
        from: tripStart
            .clone()
            .add(2, 'd')
            .hour(6)
            .minute(0),
        to: tripStart
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 3, 12:00 - 23:50
        id: 7,
        from: tripStart
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0),
        to: tripStart
            .clone()
            .add(2, 'd')
            .hour(23)
            .minute(50)
    }
];

export const result4 = [
    {
        // Day 1, 8:10 - 8:20
        id: 0,
        from: tripStart
            .clone()
            .hour(8)
            .minute(10),
        to: tripStart
            .clone()
            .hour(8)
            .minute(20)
    },
    {
        // Day 1, 8:20 - 8:30
        id: 1,
        from: tripStart
            .clone()
            .hour(8)
            .minute(20),
        to: tripStart
            .clone()
            .hour(8)
            .minute(30)
    },
    {
        // Day 1, 8:30 - 13:20
        id: 2,
        from: tripStart
            .clone()
            .hour(8)
            .minute(30),
        to: tripStart
            .clone()
            .hour(13)
            .minute(20)
    },
    {
        // Day 2, 6:30 - 9:10
        id: 3,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(6)
            .minute(30),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(9)
            .minute(10)
    },
    {
        // Day 2, 10:30 - 12:00
        id: 4,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(10)
            .minute(30),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 2, 18:00 - 19:00
        id: 5,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(18)
            .minute(0),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(19)
            .minute(0)
    },
    {
        // Day 3, 6:00 - 12:00
        id: 6,
        from: tripStart
            .clone()
            .add(2, 'd')
            .hour(6)
            .minute(0),
        to: tripStart
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 3, 12:00 - 23:50
        id: 7,
        from: tripStart
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0),
        to: tripStart
            .clone()
            .add(2, 'd')
            .hour(23)
            .minute(50)
    }
];

export const result5 = [
    {
        // Day 1, 0:00 - 0:10
        id: 0,
        from: tripStart.clone().hour(0),
        to: tripStart
            .clone()
            .hour(0)
            .minute(10)
    },
    {
        // Day 1, 0:10 - 0:20
        id: 1,
        from: tripStart
            .clone()
            .hour(0)
            .minute(10),
        to: tripStart
            .clone()
            .hour(0)
            .minute(20)
    },
    {
        // Day 1, 0:20 - 13:20
        id: 2,
        from: tripStart
            .clone()
            .hour(0)
            .minute(20),
        to: tripStart
            .clone()
            .hour(13)
            .minute(20)
    },
    {
        // Day 2, 6:30 - 9:10
        id: 3,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(6)
            .minute(30),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(9)
            .minute(10)
    },
    {
        // Day 2, 10:30 - 12:00
        id: 4,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(10)
            .minute(30),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 2, 18:00 - 19:00
        id: 5,
        from: tripStart
            .clone()
            .add(1, 'd')
            .hour(18)
            .minute(0),
        to: tripStart
            .clone()
            .add(1, 'd')
            .hour(19)
            .minute(0)
    },
    {
        // Day 3, 6:00 - 12:00
        id: 6,
        from: tripStart
            .clone()
            .add(2, 'd')
            .hour(6)
            .minute(0),
        to: tripStart
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 3, 12:00 - 23:50
        id: 7,
        from: tripStart
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0),
        to: tripStart
            .clone()
            .add(2, 'd')
            .hour(23)
            .minute(50)
    }
];
