import moment from 'moment';

export const scope = {
    from: moment.utc().startOf('d'),
    to: moment
        .utc()
        .add(4, 'd')
        .endOf('d')
};

export const plans = [
    {
        // Day 1, 9:00 - 10:00
        id: 0,
        from: scope.from.clone().hour(9),
        to: scope.from.clone().hour(10)
    },
    {
        // Day 1, 10:00 - 10:30
        id: 1,
        from: scope.from.clone().hour(10),
        to: scope.from
            .clone()
            .hour(10)
            .minute(30)
    },
    {
        // Day 1, 10:30 - 13:20
        id: 2,
        from: scope.from
            .clone()
            .hour(10)
            .minute(30),
        to: scope.from
            .clone()
            .hour(13)
            .minute(20)
    },
    {
        // Day 2, 6:30 - 9:10
        id: 3,
        from: scope.from
            .clone()
            .add(1, 'd')
            .hour(6)
            .minute(30),
        to: scope.from
            .clone()
            .add(1, 'd')
            .hour(9)
            .minute(10)
    },
    {
        // Day 2, 10:30 - 12:00
        id: 4,
        from: scope.from
            .clone()
            .add(1, 'd')
            .hour(10)
            .minute(30),
        to: scope.from
            .clone()
            .add(1, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 2, 18:00 - 19:00
        id: 5,
        from: scope.from
            .clone()
            .add(1, 'd')
            .hour(18)
            .minute(0),
        to: scope.from
            .clone()
            .add(1, 'd')
            .hour(19)
            .minute(0)
    },
    {
        // Day 3, 6:00 - 12:00
        id: 6,
        from: scope.from
            .clone()
            .add(2, 'd')
            .hour(6)
            .minute(0),
        to: scope.from
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 3, 12:00 - 23:50
        id: 7,
        from: scope.from
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0),
        to: scope.from
            .clone()
            .add(2, 'd')
            .hour(23)
            .minute(50)
    }
];

export const scope1 = {
    from: moment
        .utc()
        .year(2018)
        .dayOfYear(128)
        .startOf('d'),
    to: moment
        .utc()
        .year(2018)
        .dayOfYear(131)
        .endOf('d')
};

export const plans1 = [
    {
        // Day 1, 9:00 - 10:00
        id: 0,
        from: scope1.from.clone().hour(9),
        to: scope1.from.clone().hour(10)
    },
    {
        // Day 1, 10:00 - 10:30
        id: 1,
        from: scope1.from.clone().hour(10),
        to: scope1.from
            .clone()
            .hour(10)
            .minute(30)
    },
    {
        // Day 1, 10:30 - 13:20
        id: 2,
        from: scope1.from
            .clone()
            .hour(10)
            .minute(30),
        to: scope1.from
            .clone()
            .hour(13)
            .minute(20)
    },
    {
        // Day 2, 6:30 - 9:10
        id: 3,
        from: scope1.from
            .clone()
            .add(1, 'd')
            .hour(6)
            .minute(30),
        to: scope1.from
            .clone()
            .add(1, 'd')
            .hour(9)
            .minute(10)
    },
    {
        // Day 2, 10:30 - 12:00
        id: 4,
        from: scope1.from
            .clone()
            .add(1, 'd')
            .hour(10)
            .minute(30),
        to: scope1.from
            .clone()
            .add(1, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 2, 18:00 - 19:00
        id: 5,
        from: scope1.from
            .clone()
            .add(1, 'd')
            .hour(18)
            .minute(0),
        to: scope1.from
            .clone()
            .add(1, 'd')
            .hour(19)
            .minute(0)
    },
    {
        // Day 3, 6:00 - 12:00
        id: 6,
        from: scope1.from
            .clone()
            .add(2, 'd')
            .hour(6)
            .minute(0),
        to: scope1.from
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 3, 12:00 - 23:50
        id: 7,
        from: scope1.from
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0),
        to: scope1.from
            .clone()
            .add(2, 'd')
            .hour(23)
            .minute(50)
    }
];

export const scope2 = {
    from: moment
        .utc()
        .year(2018)
        .dayOfYear(10)
        .startOf('d'),
    to: moment
        .utc()
        .year(2018)
        .dayOfYear(11)
        .endOf('d')
};

export const plans2 = [
    {
        // Day 1, 9:00 - 10:00
        id: 0,
        from: scope2.from.clone().hour(9),
        to: scope2.from.clone().hour(10)
    },
    {
        // Day 1, 10:00 - 10:30
        id: 1,
        from: scope2.from.clone().hour(10),
        to: scope2.from
            .clone()
            .hour(10)
            .minute(30)
    },
    {
        // Day 1, 10:30 - 13:20
        id: 2,
        from: scope2.from
            .clone()
            .hour(10)
            .minute(30),
        to: scope2.from
            .clone()
            .hour(13)
            .minute(20)
    },
    {
        // Day 2, 6:30 - 9:10
        id: 3,
        from: scope2.from
            .clone()
            .add(1, 'd')
            .hour(6)
            .minute(30),
        to: scope2.from
            .clone()
            .add(1, 'd')
            .hour(9)
            .minute(10)
    },
    {
        // Day 2, 10:30 - 12:00
        id: 4,
        from: scope2.from
            .clone()
            .add(1, 'd')
            .hour(10)
            .minute(30),
        to: scope2.from
            .clone()
            .add(1, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 2, 18:00 - 19:00
        id: 5,
        from: scope2.from
            .clone()
            .add(1, 'd')
            .hour(18)
            .minute(0),
        to: scope2.from
            .clone()
            .add(1, 'd')
            .hour(19)
            .minute(0)
    }
];

export const scope3 = {
    from: moment
        .utc()
        .year(2018)
        .dayOfYear(15)
        .startOf('d'),
    to: moment
        .utc()
        .year(2018)
        .dayOfYear(22)
        .endOf('d')
};

export const plans3 = [
    {
        // Day 1, 9:00 - 10:00
        id: 0,
        from: scope3.from.clone().hour(9),
        to: scope3.from.clone().hour(10)
    },
    {
        // Day 1, 10:00 - 10:30
        id: 1,
        from: scope3.from.clone().hour(10),
        to: scope3.from
            .clone()
            .hour(10)
            .minute(30)
    },
    {
        // Day 1, 10:30 - 13:20
        id: 2,
        from: scope3.from
            .clone()
            .hour(10)
            .minute(30),
        to: scope3.from
            .clone()
            .hour(13)
            .minute(20)
    },
    {
        // Day 2, 6:30 - 9:10
        id: 3,
        from: scope3.from
            .clone()
            .add(1, 'd')
            .hour(6)
            .minute(30),
        to: scope3.from
            .clone()
            .add(1, 'd')
            .hour(9)
            .minute(10)
    },
    {
        // Day 2, 10:30 - 12:00
        id: 4,
        from: scope3.from
            .clone()
            .add(1, 'd')
            .hour(10)
            .minute(30),
        to: scope3.from
            .clone()
            .add(1, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 2, 18:00 - 19:00
        id: 5,
        from: scope3.from
            .clone()
            .add(1, 'd')
            .hour(18)
            .minute(0),
        to: scope3.from
            .clone()
            .add(1, 'd')
            .hour(19)
            .minute(0)
    },
    {
        // Day 3, 6:00 - 12:00
        id: 6,
        from: scope3.from
            .clone()
            .add(2, 'd')
            .hour(6)
            .minute(0),
        to: scope3.from
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 3, 12:00 - 23:50
        id: 7,
        from: scope3.from
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0),
        to: scope3.from
            .clone()
            .add(2, 'd')
            .hour(23)
            .minute(50)
    }
];

export const scopeV1 = {
    from: moment.utc().startOf('d'),
    to: moment
        .utc()
        .add(4, 'd')
        .endOf('d')
};

export const plansV1 = [
    {
        // Day 2, 9:00 - 10:00
        id: 0,
        from: scopeV1.from
            .clone()
            .add(1, 'd')
            .hour(9),
        to: scopeV1.from
            .clone()
            .add(1, 'd')
            .hour(10)
    },
    {
        // Day 2, 10:00 - 10:30
        id: 1,
        from: scopeV1.from
            .clone()
            .add(1, 'd')
            .hour(10),
        to: scopeV1.from
            .clone()
            .add(1, 'd')
            .hour(10)
            .minute(30)
    },
    {
        // Day 2, 10:30 - 13:20
        id: 2,
        from: scopeV1.from
            .clone()
            .add(1, 'd')
            .hour(10)
            .minute(30),
        to: scopeV1.from
            .clone()
            .add(1, 'd')
            .hour(13)
            .minute(20)
    },
    {
        // Day 3, 6:30 - 9:10
        id: 3,
        from: scopeV1.from
            .clone()
            .add(2, 'd')
            .hour(6)
            .minute(30),
        to: scopeV1.from
            .clone()
            .add(2, 'd')
            .hour(9)
            .minute(10)
    },
    {
        // Day 3, 10:30 - 12:00
        id: 4,
        from: scopeV1.from
            .clone()
            .add(2, 'd')
            .hour(10)
            .minute(30),
        to: scopeV1.from
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 5, 12:00 - 23:50
        id: 7,
        from: scopeV1.from
            .clone()
            .add(4, 'd')
            .hour(12)
            .minute(0),
        to: scopeV1.from
            .clone()
            .add(4, 'd')
            .hour(23)
            .minute(50)
    }
];

export const scopeV1_1 = {
    from: moment
        .utc()
        .year(2014)
        .dayOfYear(123)
        .startOf('d'),
    to: moment
        .utc()
        .year(2014)
        .dayOfYear(125)
        .endOf('d')
};

export const plansV1_1 = [
    {
        // Day 1, 9:00 - 10:00
        id: 0,
        from: scopeV1_1.from.clone().hour(9),
        to: scopeV1_1.from.clone().hour(10)
    },
    {
        // Day 1, 10:00 - 10:30
        id: 1,
        from: scopeV1_1.from.clone().hour(10),
        to: scopeV1_1.from
            .clone()
            .hour(10)
            .minute(30)
    },
    {
        // Day 1, 10:30 - 13:20
        id: 2,
        from: scopeV1_1.from
            .clone()
            .hour(10)
            .minute(30),
        to: scopeV1_1.from
            .clone()
            .hour(13)
            .minute(20)
    },
    {
        // Day 2, 6:30 - 9:10
        id: 3,
        from: scopeV1_1.from
            .clone()
            .add(1, 'd')
            .hour(6)
            .minute(30),
        to: scopeV1_1.from
            .clone()
            .add(1, 'd')
            .hour(9)
            .minute(10)
    },
    {
        // Day 2, 10:30 - 12:00
        id: 4,
        from: scopeV1_1.from
            .clone()
            .add(1, 'd')
            .hour(10)
            .minute(30),
        to: scopeV1_1.from
            .clone()
            .add(1, 'd')
            .hour(12)
            .minute(0)
    },
    {
        // Day 3, 12:00 - 23:50
        id: 7,
        from: scopeV1_1.from
            .clone()
            .add(2, 'd')
            .hour(12)
            .minute(0),
        to: scopeV1_1.from
            .clone()
            .add(2, 'd')
            .hour(23)
            .minute(50)
    }
];
