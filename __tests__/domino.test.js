import adjustmentDomino from '../algs/adjustmentDomino';

import { tripAttr, plans, result1, result2, result3, result4, result5 } from '../__testConstants__/domino.constants';

describe('adjusts-plans-via-slider', () => {
    it('first-from-1hr', () => {
        expect(adjustmentDomino(plans, 0, 1, 60, 'from', false, tripAttr.tripStart, tripAttr.tripEnd)).toEqual(result1);
    });
    it('first-to-1hr', () => {
        expect(adjustmentDomino(plans, 0, 1, 60, 'to', false, tripAttr.tripStart, tripAttr.tripEnd)).toEqual(result2);
    });
    it('second-to-2hr', () => {
        expect(adjustmentDomino(plans, 1, 1, 120, 'to', false, tripAttr.tripStart, tripAttr.tripEnd)).toEqual(result3);
    });
    it('second-to-negative-2hr', () => {
        expect(adjustmentDomino(plans, 1, -1, 120, 'to', false, tripAttr.tripStart, tripAttr.tripEnd)).toEqual(result4);
    });
    it('second-to-negative-10hr10min', () => {
        expect(adjustmentDomino(plans, 1, -1, 610, 'to', false, tripAttr.tripStart, tripAttr.tripEnd)).toEqual(result5);
    });
    it('second-to-negative-12hr', () => {
        expect(adjustmentDomino(plans, 1, -1, 720, 'to', false, tripAttr.tripStart, tripAttr.tripEnd)).toEqual(null);
    });
    it('first-from-30hr', () => {
        expect(adjustmentDomino(plans, 0, 1, 3600, 'from', false, tripAttr.tripStart, tripAttr.tripEnd)).toEqual(null);
    });
    it('first-from-negative-30hr', () => {
        expect(adjustmentDomino(plans, 0, -1, 3600, 'from', false, tripAttr.tripStart, tripAttr.tripEnd)).toEqual(null);
    });
});
