import shiftPlans from '../algs/shiftPlans';
// eslint-disable camel-case
import {
    scope,
    plans,
    scope1,
    plans1,
    scope2,
    plans2,
    scope3,
    plans3,
    plansV1,
    scopeV1,
    plansV1_1,
    scopeV1_1
} from '../__testConstants__/shift.constants';

describe('shifts-plans-to-new-scope', () => {
    it('shifts-smaller-duration:no-conflict', () => {
        expect(shiftPlans(scope, scope1, plans).newPlans).toMatchObject(plans1);
    });
    it('shifts-smaller-duration:conflict', () => {
        expect(shiftPlans(scope, scope2, plans).newPlans).toMatchObject(plans2);
    });
    it('shifts-larger-duration:no-conflict', () => {
        expect(shiftPlans(scope, scope3, plans).newPlans).toMatchObject(plans3);
    });
    it('shifts-smaller-duration:clear-empty', () => {
        expect(shiftPlans(scopeV1, scopeV1_1, plansV1).newPlans).toMatchObject(plansV1_1);
    });
});
